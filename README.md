# git-push-current

# Usage

```bash
git push-current -u origin
```

# Installation

```bash
make DESTDIR=/usr/local install
```
