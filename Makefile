.DEFAULT_GOAL := install_link
DESTDIR ?= $(HOME)/.local
INSTALL_BIN_DIR := $(DESTDIR)/bin

scripts := $(shell find . -mindepth 1 -maxdepth 1 -type f -executable)

$(INSTALL_BIN_DIR):
	[ -d $(INSTALL_BIN_DIR) ] || mkdir -p $(INSTALL_BIN_DIR)

install_link: $(INSTALL_BIN_DIR) $(scripts)
	$(foreach f,$(scripts),ln -f -s "$(shell pwd)/$(shell basename $(f))" "$(INSTALL_BIN_DIR)"/"$(shell basename "$(f)")";)

install: $(scripts)
	$(foreach f,$(scripts),install -Dm0755 -t $(INSTALL_BIN_DIR) $(f);)

.PHONY: install_link install
